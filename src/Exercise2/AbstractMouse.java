package Exercise2;

public abstract  class AbstractMouse {

	public int x, y;
	public String direction;
	public String color;
	
	
	public AbstractMouse(int x, int y, String direction, String color) {
		
		this.x = x;
		this.y = y;
		this.direction = direction;
		this.color = color;
	}
	
	public abstract void nextMove(Space s);
	
	
	public void move(){
		if (this.direction.compareTo("Up")==0)
			if(this.x==0)
				this.x=6;
			else
				this.x--;
		
		if (this.direction.compareTo("Down")==0)
			if(this.x==6)
				this.x=0;
			else
				this.x++;
		
		if (this.direction.compareTo("Left")==0)
			if(this.y==0)
				this.y=6;
			else
				this.y--;
		
		if (this.direction.compareTo("Right")==0)
			if(this.y==6)
				this.y=0;
			else
				this.y++;
		
	}

	@Override
	public String toString() {
		
		String class_name = this.getClass().getName();
		return  class_name + "[x=" + x + ", y=" + y + ", direction=" + direction + ", color=" + color + "]";
	}
	
	
	
}
