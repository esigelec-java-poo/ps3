package Exercise2;

public class PetMouse extends AbstractMouse  {
	
	public PetMouse(int x, int y, String direction, String color) {
		super(x, y, direction, color);
		// TODO Auto-generated constructor stub
	}


	@Override
	public void nextMove(Space s) {
		// TODO Auto-generated method stub
		
		this.move();
		
		String cell_color = s.getCellColor(this.x, this.y);
		if(cell_color.compareTo("gray")==0) {
			
			String mouse_direction = this.direction;
			if(mouse_direction.compareTo("Up")==0)
				this.direction = "Right";
			
			if(mouse_direction.compareTo("Right")==0)
				this.direction = "Down";
			
			if(mouse_direction.compareTo("Down")==0)
				this.direction = "Left";
			
			if(mouse_direction.compareTo("Left")==0)
				this.direction = "Up";
			
			s.setCellColor(this.x, this.y, this.color);
			
		}
		
		else {
			
			String mouse_direction = this.direction;
			if(mouse_direction.compareTo("Up")==0)
				this.direction = "Left";
			
			if(mouse_direction.compareTo("Right")==0)
				this.direction = "Up";
			
			if(mouse_direction.compareTo("Down")==0)
				this.direction = "Right";
			
			if(mouse_direction.compareTo("Left")==0)
				this.direction = "Down";
			
			
			s.setCellColor(this.x, this.y, "gray");
			
		}
		
		
		
	
	}
		

}
