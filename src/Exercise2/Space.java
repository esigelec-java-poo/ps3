package Exercise2;

public class Space {
	
	 public String [][] cells_colors;

	public Space(String[][] cells_colors) {
		super();
		this.cells_colors = cells_colors;
		
	}
	
	
	public void setCellColor(int x, int y, String color){
		this.cells_colors[x][y]= color;
	}
	
	public String getCellColor(int x, int y){
		
		return this.cells_colors[x][y];
		
	}
	 
	 

	 
	 
}
